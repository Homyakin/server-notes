# Предназначение

Здесь описаны разные инструменты для работы с памятью

## Список
- [clear_cache.sh](clear_cache.sh) -- освобождает оперативную память, запускать по cron'у
- [clear_docker_old_files.sh](clear_docker_old_files.sh) -- удаляет файлы от старых docker-контейнеров, запускать по cron'у
- [limit_journal.md](limit_journal.md) - ограничить размер журнала